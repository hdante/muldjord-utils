---------------
A Troubled Mind
---------------

A few hundred yards is all I have left.
This is the last walk for me,
before my final rest...

I will tell you of a life, full of pain and despair.
But also a life, with more nourish than tear.

It might seem bleak, sad and bereft of glory,
but it suits me well,
so here is my story:

As a young boy, my eyes were painted in red,
Everywhere I looked, I saw horror instead.

When I grew older, I learned to cope,
Understand it better,
that was my hope...

From my feet, through my spine to my neck,
a tickling sensation exploding in dread.
It seems to fade away, but I know it is there,
my friend of decay!

To a stranger it is hard to explain,
hard to understand, when dread is not your domain.
A force that is seen by none but myself,
a never ending struggle, with the cards,
I’ve been dealt!

It was tough my dear,
when you came along for the ride.

But it was worth it,
now that you walk by my side...

---



Lyrics Copyright 2011 by Muldjord